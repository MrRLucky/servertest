#A sample server implementation
================================================


### For details please read the **overview.pdf** in the source folder.

### The executable **server.jar** can be found in the source folder as well.

### There is also a dockerfile and a startupscript.sh in the source folder:

**Build Dockerfile:**

*Note: The startupscript.sh must be in the same folder*
```
#!bash
docker build --tag rl/mongoserver .
```
*Note: At least on windows systems an absolut path had to be used for mapping volumes*
```
#!bash
Example execution:
docker run -p 8080:8080 -v /absolutPathTo/www:/data/www --name server rl/mongoserver
```

### The index.html containing the simple **wall client** can be found at the Source/www.

### Based on Pooling-web-server by jrudolph (Johannes Rudolph) available on Github ([https://github.com/jrudolph/Pooling-web-server](https://github.com/jrudolph/Pooling-web-server)).



## Thank you and have fun reviewing!