FROM ubuntu:latest
MAINTAINER RobinLuckey 

# Source od following code: https://devops.profitbricks.com/tutorials/creating-a-mongodb-docker-container-with-an-attached-storage-volume/

RUN \
   apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv 7F0CEB10 && \
   echo 'deb http://downloads-distro.mongodb.org/repo/ubuntu-upstart dist 10gen' | tee /etc/apt/sources.list.d/mongodb.list && \
   apt-get update && \
   apt-get install -y mongodb-org

VOLUME ["/data/db"]
WORKDIR /data

# Own code

RUN apt-get update
RUN apt-get install -y openjdk-8-jdk
COPY server.jar /home/server.jar

EXPOSE 27017

EXPOSE 8080

COPY startupScript.sh /home/startupScript.sh

CMD /bin/bash /home/startupScript.sh