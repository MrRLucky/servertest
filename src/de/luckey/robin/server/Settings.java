package de.luckey.robin.server;

import java.net.InetSocketAddress;
import java.net.SocketAddress;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
/**
 * @author Johannes Rudolph
 * @author Robin Luckey changed type from abstract class to singleton class in order to allow  
 * overriding the settings using commandline parameters. 
 * Added some constants.
 *
 */
public class Settings {

	// This is the most important configuration to do and you would have to find
    // out experimentally which is the setting for the pool.
    // Without keep-alive and when the handling of request is mainly CPU-bound
    // you would use about N threads (with N being the number of CPUs).
    // When processing requests contains wait times (e.g. because of I/O) you
    // would have to increase the number of threads to N*(1+WT/ST) with WT/ST being the ratio
    // of wait time to service time.
    //
    // E.g. see Brian Goetz, Java theory and practice: Thread pools and work queues
    // http://www.ibm.com/developerworks/java/library/j-jtp0730.html
    //
    // With keep-alive in place, configuration becomes harder because you have
    // to figure out both, keep-alive timeouts and the number of threads. With a long too long timeout
    // you might keep open many connections and threads waiting for requests and using system resources.
    //
    // One possibility is to decouple connections and threads by putting open, waiting connections into
    // a list which is monitored for new data. When new data arrives the requests are scheduled again
    // for processing. (not implemented here)
	
	private static Settings instance;
	
    public ExecutorService createExecutor() {
        return Executors.newFixedThreadPool(threadpoolSize);
    }
    /**
     * number of threads
     */
    private int threadpoolSize = 1;
    /**
     * The root directory
     */
    private String root = "www";    
    /**
     * The port the server should be using
     */
    private int serverPort = 8080;
    /**
     * The socket timeout between connection accept and expecting the first request.
     */
    private int firstReadTimeout = 5000;
    /**
     * The timeout when waiting for headers.
     */
    private int headerTimeout = 2000;
    /**
     * The timeout in keep-alive connections when waiting for the next request
     */
    private int keepAliveTimeout = 20000;
	/**
	 * Port of the MongoDB
	 */
    private int dbPort = 27017;
    /**
     * the host of the data base
     */
	private String databaseHost = "localhost";

	public int getThreadpoolSize() {
		return threadpoolSize;
	}
	/**
	 * only call prior to starting the server 
	 * @param threadpoolSize  max number of threads
	 */
	public void setThreadpoolSize(int threadpoolSize) {
		this.threadpoolSize = threadpoolSize;
	}

	public String getRoot() {
		return root;
	}

	public void setRoot(String root) {
		this.root = root;
	}

	public int getServerPort() {
		return serverPort;
	}
	public void setServerPort(int serverPort) {
		this.serverPort = serverPort;
	}
	/**
	 * Creates a SocketAdress
	 * @return the SocketAdress
	 */
	public SocketAddress getEndpoint() {
		return new InetSocketAddress(serverPort);
	}

	public int getFirstReadTimeout() {
		return firstReadTimeout;
	}
	/**
	 * only call prior to starting the server
	 * @param firstReadTimeout socket time when handling starts
	 */
	public void setFirstReadTimeout(int firstReadTimeout) {
		this.firstReadTimeout = firstReadTimeout;
	}

	public int getHeaderTimeout() {
		return headerTimeout;
	}
	/**
	 * only call prior to starting the server
	 * @param headerTimeout timeout when reading the headers
	 */
	public void setHeaderTimeout(int headerTimeout) {
		this.headerTimeout = headerTimeout;
	}
	
	public int getKeepAliveTimeout() {
		return keepAliveTimeout;
	}
	/**
	 * only call prior to starting the server
	 * @param keepAliveTimeout timeout when reading the headers
	 */
	public void setKeepAliveTimeout(int keepAliveTimeout) {
		this.keepAliveTimeout = keepAliveTimeout;
	}

	public int getDBPort() {
		return dbPort;
	}
	/**
	 * only call prior to starting the server
	 * @param the database port
	 */
	public void setDBPort(int dbPort) {
		this.dbPort = dbPort;
	}

	/*
	 * singleton, thus private contructor
	 */
	private Settings(){};
	
	/**
	 * creates new instance of the MongoDB class or returns the instance if it already exists
	 * @return an instance of this class
	 */
	public static Settings getInstance() {
		if(instance == null) {
			synchronized (Settings.class) {
				if(instance == null) {
					instance = new Settings();
				}
			}
		}
		return instance;
	}

	public String getDatabaseHost() {
		return databaseHost;
	}
	/**
	 * only call prior to starting the server
	 * @param databaseHost host of database
	 */
	public void setDatabaseHost(String databaseHost) {
		this.databaseHost = databaseHost;
	}
}
