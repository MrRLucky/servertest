package de.luckey.robin.server;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;

import de.luckey.robin.handler.HttpHandler;
import de.luckey.robin.util.FileUtils;
import de.luckey.robin.util.Logging;

/**
 * A simple pooling web server. It waits on the main thread for new connections and
 * then schedules them for processing with one of the threads from the pool.
 *
 * You can define the behaviour by setting executor and handler.
 * Field `executor` specifies the pooling strategy to use. The handler is called in its own
 * thread to handle an incoming connection.
 * @author Johannes Rudolph
 * @author Robin Luckey added command line argument handling
 * also changed the call() method to create a HttpHandler
 */
public class SimplePooledWebServer {
	Settings settings = Settings.getInstance();
    private  ExecutorService executor = settings.createExecutor();
    public void run() throws IOException {
       
		ServerSocket theServer = new ServerSocket();
        theServer.bind(settings.getEndpoint());
        // Main server Loop
        while(true) {
            final Socket client = theServer.accept();
            Logging.log("\nNew connection: %s", client);
            executor.submit(new Callable<Void>() {
                @Override
                public Void call() throws Exception {
                    try {
                    	// Call the Handler
                        new HttpHandler().handleConnection(client);
                    } catch (IOException exception) {
                        System.err.println("Error when handling request: "+exception.getMessage());
                        exception.printStackTrace(System.err);
                    } finally {
                        if (!client.isClosed())
                            client.close();
                    }
                    return null;
                }
            });
        }
    }

    public static void main(String[] args) throws IOException {
    	Settings settings = Settings.getInstance();
    	int length = args.length;
    	switch (length) {
	    	case 5: {
	    		int threadpoolSize = Integer.parseInt(args[4]);
	    		settings.setThreadpoolSize(threadpoolSize);
	    	}
	    	case 4: {
	    		int databasePort = Integer.parseInt(args[3]);
	    		settings.setDBPort(databasePort);
	    	}
	    	case 3: {
	    		String databaseHost = args[2];
	    		settings.setDatabaseHost(databaseHost);
	    	}
	    	case 2: {
	    		int serverPort = Integer.parseInt(args[1]);
	    		settings.setServerPort(serverPort);
	    	}
	    	case 1: {
	    		String webroot = args[0];
	    		webroot = FileUtils.createDir(webroot);
	    		settings.setRoot(webroot);
	    		break;
	    	}
	    	case 0: {
	    		String webroot = FileUtils.createDir(settings.getRoot());
	    		settings.setRoot(webroot);
	    	}
    	}
    	Logging.log("%s", "Starting server on Port: " + settings.getServerPort());
        Logging.log("%s", "Threadpool size: " + settings.getThreadpoolSize());
        Logging.log("%s", "Webroot: " + settings.getRoot());
        new SimplePooledWebServer().run();
                
    }
    
    
}
