package de.luckey.robin.test;

import static org.junit.Assert.assertEquals;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;

import de.luckey.robin.controller.HeaderProcessor;
import de.luckey.robin.util.HeadersEnum;
/**
 * Tests the header evaluation
 * First the correct behavior when all tests are skipped is tested
 * Then the individual conditions are tested by skipping the conditions
 * that are not currently tested.
 *  
 * @author Robin Luckey
 *
 */
public class HeaderTest {
	// Sample file eTag, the header processor handles file and directory  
	final String serverEtag="d623032f55d85538336ba541f45209e6";

	@Before
	public void setUp() throws Exception {
		
	}

	/**
	 * Helper class to easily creat a set of headers
	 * @param match pass true if a Match header be included
	 * @param noneMatch pass true if a None-Match header be included
	 * @param modifiedSince pass a date String (valid format: "EEE, dd MMM yyyy HH:mm:ss zzz"), if a modified-since header should be included
	 * @param eTag pass an etag, a sample etag is defined int the constants at the top of this file 
	 * @return
	 */
	private Map<String, String> createHeadersMap (boolean match, boolean noneMatch, String modifiedSince, String eTag) {
		Map<String, String> headers = new HashMap<>();
		if(match) {
			headers.put(HeadersEnum.IF_MATCH.stringValue(), eTag);
		}
		if(noneMatch) {
			headers.put(HeadersEnum.IF_NONE_MATCH.stringValue(), eTag);
		}
		if(!modifiedSince.equals("")) {
			headers.put(HeadersEnum.IF_MODIFIED_SINCE.stringValue(), modifiedSince);
		}
		return headers;
	}
	
	/**
	 * 
	 * @param inputDate
	 * @return
	 */
	private Date createDate(String inputDate) {
		SimpleDateFormat format = new SimpleDateFormat("EEE, dd MMM yyyy HH:mm:ss zzz", Locale.ENGLISH);
		try {
			return format.parse(inputDate);
		} catch (ParseException e) {
			System.out.println("date could not be created");
		}
		return null;
	}
	/**
	 * This is the basis for the other tests. Since it tests the skipping of conditions when headers are not present
	 */
	@Test
	public void testGetHttpStatus() {
		//Empty headers
		Map<String, String> emptyHeaders = createHeadersMap(false, false, "", "");
		assertEquals("200", new HeaderProcessor(emptyHeaders, "", null).getHttpStatus().asText());
		
	}
	/**
	 * Tests the If-Match header evaluation
	 */
	@Test
	public void testIfMatch() {
		// requested etag does equal file etag
		Map<String, String> headers = createHeadersMap(true, false, "", serverEtag);
		assertEquals("200", new HeaderProcessor(headers, serverEtag, null).getHttpStatus().asText());
		
		// requested etag does not equal file etag
		headers = createHeadersMap(true, false, "", serverEtag);
		String receivedEtag = "notMatchingEtag";
		assertEquals("412", new HeaderProcessor(headers, receivedEtag, null).getHttpStatus().asText());
		
		// multiple etags with ONE matching
		headers = createHeadersMap(true, false, "", "notMatchingEtag" + ", " + serverEtag + ", " + "notMatchingEtag2");
		receivedEtag = serverEtag;
		assertEquals("200", new HeaderProcessor(headers, receivedEtag, null).getHttpStatus().asText());
		
		// multiple etags none matches the server-etag  
		headers = createHeadersMap(true, false, "", "notMatchingEtag" + "," + "notMatchingEtag2" + "," + "notMatchingEtag3" );
		receivedEtag = "FILE_ETAG";
		assertEquals("412", new HeaderProcessor(headers, receivedEtag, null).getHttpStatus().asText());
	}
	/**
	 * Tests the If-None-Match header evaluation
	 */
	@Test
	public void testIfNoneMatch() {		
		// The etags do match
		Map<String, String> headers = createHeadersMap(false, true, "", serverEtag);		
		assertEquals("304", new HeaderProcessor(headers, serverEtag, null).getHttpStatus().asText());
		
		// The etags do not match
		headers = createHeadersMap(false, true, "", serverEtag);		
		assertEquals("200", new HeaderProcessor(headers, "notMatchingEtag", null).getHttpStatus().asText());	
		
		// multiple etags with ONE matching
		headers = createHeadersMap(false, true, "", "notMatchingEtag" + ", " + serverEtag + ", " + "notMatchingEtag2");
		String receivedEtag = serverEtag;
		assertEquals("304", new HeaderProcessor(headers, serverEtag, null).getHttpStatus().asText());
				
		// multiple etags none matches the server-etag  
		headers = createHeadersMap(false, true, "", "notMatchingEtag" + "," + "notMatchingEtag2" + "," + "notMatchingEtag3" );
		receivedEtag = "FILE_ETAG";
		assertEquals("200", new HeaderProcessor(headers, receivedEtag, null).getHttpStatus().asText());
	}
	/**
	 * Tests the If-Modified-Since header evaluation
	 */
	@Test
	public void testIfModifiedSinceMatch() {		
		// Same Date
		Map<String, String> headers = createHeadersMap(false, false, "Sat, 29 Oct 1994 19:43:31 GMT", "");
		
		assertEquals("304", new HeaderProcessor(headers, "", createDate("Sat, 29 Oct 1994 19:43:31 GMT")).getHttpStatus().asText());
		
		// requested file newer
		headers = createHeadersMap(false, false, "Tue, 9 May 2017 00:00:00 CEST", "");
		assertEquals("200", new HeaderProcessor(headers, "", createDate("Tue, 9 May 2017 12:00:00 CEST")).getHttpStatus().asText());
		
		// requested file older
		headers = createHeadersMap(false, false, "Tue, 9 May 2017 12:00:00 CEST", "");
		assertEquals("304", new HeaderProcessor(headers, "", createDate("Tue, 9 May 2017 00:00:00 CEST")).getHttpStatus().asText());
		
		// invalid date in header
		headers = createHeadersMap(false, false, "invalidDate", "");
		assertEquals("200", new HeaderProcessor(headers, "", createDate("Tue, 9 May 2017 00:00:00 CEST")).getHttpStatus().asText());
		
	}
	
	/**
	 *  Tests the combination of If-None-Match * and If-Modified-Since behavior
	 */
	@Test
	public void testIfModifiedSinceIfNoneMatch() {
		// * + requested file newer
		Map<String, String> headers = createHeadersMap(false, false, "Tue, 9 May 2017 00:00:00 CEST", "*");
		assertEquals("200", new HeaderProcessor(headers, "", createDate("Tue, 9 May 2017 12:00:00 CEST")).getHttpStatus().asText());
		
		// requested file older
		headers = createHeadersMap(false, false, "Tue, 9 May 2017 12:00:00 CEST", "*");
		assertEquals("304", new HeaderProcessor(headers, "", createDate("Tue, 9 May 2017 00:00:00 CEST")).getHttpStatus().asText());
	}

}
