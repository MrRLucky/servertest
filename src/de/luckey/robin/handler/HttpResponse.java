package de.luckey.robin.handler;

import java.io.IOException;
import java.io.OutputStream;
/**
 * Class Result represents the response which has to be sent back
 * Implementors have to implement addHeaders and writeBody.
 * @author Johannes Rudolph
 * @author Robin Luckey extracted this class - formerly named "Result" - from the HttpHandler
 * So that it can be used without a reference to HttpHandler
 *
 */
public abstract class HttpResponse {
	private final String resultCode;
    private final StringBuffer headerBuffer = new StringBuffer();
    
    public HttpResponse(String resultCode) {
        this.resultCode = resultCode;
    }
    public StringBuffer getHeaderBuffer() {
		return headerBuffer;
	}
	public String getResultCode() {
        return resultCode;
    }
    public String getHeaders() {
        return headerBuffer.toString();
    }

    public void addResponseHeader(String header, String value) {
        headerBuffer
            .append(header)
            .append(": ")
            .append(value)
            .append("\r\n");
    }
    /**
     * the header to be written to the Response
     */
    protected abstract void addHeaders();
    /**
     * The Body to be sent back
     * @param os the output stream of the socket
     * @throws IOException	if writing fails
     */
    protected abstract void writeBody(OutputStream os) throws IOException;
}
