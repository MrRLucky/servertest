package de.luckey.robin.handler;

import static de.luckey.robin.util.Logging.log;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.net.Socket;
import java.net.SocketTimeoutException;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import de.luckey.robin.controller.GetWallpostsController;
import de.luckey.robin.controller.PostToWallController;
import de.luckey.robin.controller.StaticFileController;
import de.luckey.robin.server.Settings;
import de.luckey.robin.util.HeadersEnum;

/**
 * An HttpHandler class which does basic parsing and then delegates the request to a controller.
 * @author Johannes Rudolph 
 * @author Robin Luckey Removed abstract modifier since this class is now instantiate by the main server loop.
 * Added "POST" as request method, request body reading and delegation to controller.
 * Extracted inner class Result to HttpResponse class. 
 */
public class HttpHandler implements Handler{

	private void fail(Writer writer, String code) throws IOException {
        writer.append("HTTP/1.0 ")
            .append(code)
            .append("\r\n");
    }
	// pattern use to match headerline of the request
    private final static Pattern HeaderLine = Pattern.compile("([^:]*):\\s*(.*)");
    /**
     * Reads the Headers of a request 
     * @param reader
     * @return A Map containing the Headers
     * @throws IOException
     */
    private Map<String, String> readHeaders(BufferedReader reader) throws IOException {
        Map<String, String> headers = new LinkedHashMap<String, String>();

        int length = 0;
        do {
            String line = reader.readLine();
            length = line.length();
            if (length > 0) {
                Matcher matcher = HeaderLine.matcher(line);
                if (matcher.matches())
                    headers.put(matcher.group(1), matcher.group(2).toLowerCase());
                else
                    log("Skipping invalid header: '%s'", line);
            }
        } while (length > 0) ;

        return headers;
    }
    /**
     * Implements the keep alive behavior for http 1.0 and 1.1
     * @param headers the request headers
     * @param version the request version
     * @return returns true if Version is http 1.0 and connection header contains "close" or if Version is http is 1.1 and connection header equals "close"
     */
    private boolean shouldKeepAlive(Map<String, String> headers, String version) {
    	
        boolean hasConnectionKeepAlive =
            headers.containsKey("Connection") &&
            headers.get("Connection").contains("keep-alive");
        
        boolean hasConnectionClose =
            headers.containsKey("Connection") &&
            headers.get("Connection").contains("close");

        // Keep-Alive in HTTP/1.0: Not specified but implementations use
        // header "Connection: Keep-Alive" to flag a persistent connection.
        // The receiver has to acknowledge the persistent connection with a response header
        // "Connection: Keep-Alive" and possibly additional information with the header
        // "Keep-Alive: [...]"
        // see http://ftp.ics.uci.edu/pub/ietf/http/hypermail/1995q4/0063.html
        // and RFC 2068 ("19.7.1 Compatibility with HTTP/1.0 Persistent Connections")
        //
        // In HTTP/1.1 (RFC 2616): A connection is considered persistent
        // by default. The sender can flag a connection close with the request header
        // "Connection: close"
        return
            ("1.0".equals(version) &&
             hasConnectionKeepAlive
            ) ||
            ("1.1".equals(version) &&
             !hasConnectionClose);
    }
    /**
     * Sets timeout and handles the request
     * @param client
     * @return
     * @throws IOException
     */
    @Override
    public boolean handleConnection(Socket client) throws IOException {
        client.setSoTimeout(Settings.getInstance().getFirstReadTimeout());

        return readAndDelegate(client);
    }
    // Pettern for matching if request contains is valid
    final static Pattern GETHEADPOSTRequest = Pattern.compile("(GET|HEAD|POST) ([^ ]+) HTTP/(\\d\\.\\d)");
    
    /**
     * Reads the Request header and body and Delegates it to the correct Controller
     * @param client
     * @return
     * @throws IOException
     */
    private boolean readAndDelegate(Socket client) throws IOException {
        // FIXME: to read request bodies we would have to make sure to use correct encoding here
    	final OutputStream os = client.getOutputStream();
    	final Writer writer = new OutputStreamWriter(os);      
        
        BufferedReader reader = new BufferedReader(new InputStreamReader(client.getInputStream()));
        
        // Request processing
        try {
        	String requestLine = reader.readLine();
            
        	log("Serving Request '%s", requestLine);
            // Headers have to be send continuously without to much pauses in between
            client.setSoTimeout(Settings.getInstance().getHeaderTimeout());
            Map<String, String> headers = readHeaders(reader);
           
            
            Matcher lineMatcher = GETHEADPOSTRequest.matcher(requestLine);
           
            if (lineMatcher.matches()) {
                String method = lineMatcher.group(1);
                String uri = lineMatcher.group(2);
                String version = lineMatcher.group(3);
                if (!("1.0".equals(version) || "1.1".equals(version))) {
                	fail(writer, "501 Version not implemented "+version);
            	}
                else {
	                HttpRequest request = new HttpRequest(method, uri, version, headers);
	                HttpResponse response = null;
	                boolean onlyHeader = false;
	                
	                // Read body of post request
	                if(method.equals("POST")) {
	                	String headerName = HeadersEnum.CONTENT_LENGTH.stringValue();
	
	                	if(headers.containsKey(headerName)) {    	
	            			request.setBody(readBody(reader, headers.get(headerName)));
	                	}
	                	else {
	                		System.err.printf("Bad Request: '%s'\n", requestLine);
	                        fail(writer, "400 Bad Request");
	                	}
	                }
	                // Delegate Request to correct Controller
	                response = delegate(request);
	                
	                response.addHeaders();
	                
	                // Write Response to Socket
	                writer.append("HTTP/")
	                .append(version)
	                .append(' ')
	                .append(response.getResultCode())
	                .append("\r\n")
	                .append(response.getHeaders());
	                
	                boolean keepAlive = shouldKeepAlive(headers, version);
	                if (keepAlive && "1.0".equals(version))
	                	writer.append("Connection: keep-alive\r\n");
	                else if (!keepAlive && "1.1".equals(version))
	                	writer.append("Connection: close\r\n");
	                
	                writer.append("\r\n");
	                	                
	                writer.flush();
	                if (!onlyHeader && response != null) {
	                	response.writeBody(os);	                	
	                }
	                
	                return keepAlive;
                }
            }
            else {
            System.err.printf("Bad Request: '%s'\n", requestLine);
            fail(writer, "400 Bad Request");
            }
        } catch(SocketTimeoutException e) {
            fail(writer, "408 Request Timeout");
        }

        writer.flush();
        writer.close();
        
        return false;
    }
    /**
     * Delegates the request to the correct Controller
     * @param request	the request to delegate
     * @return	httpResponse to be returned to client
     */
	private HttpResponse delegate(HttpRequest request) {
		HttpResponse response;
		if(request.getUri().equals("/wallpost")) {
			response = new PostToWallController().serve(request);
		}
		else if(request.getUri().equals("/getposts")){
				response = new GetWallpostsController().serve(request);
		}
		else {	                		
			response = new StaticFileController().serve(request);
		}
		return response;
	}
    /**
     * reads the body of a request from the Socket 
     * @param reader BufferedReader for reading from Socket
     * @param contentLengthHeader contains the content length header value read from request
     * @return
     * @throws IOException
     */
	private String readBody(BufferedReader reader, String contentLengthHeader)
			throws IOException {
		int contentLength = Integer.parseInt(contentLengthHeader);
		
		StringBuilder tmpStringBuilder = new StringBuilder("");
		char tmpByte;
		for(int i = 0 ; i < contentLength; i++) {
			tmpByte = (char) reader.read();
			tmpStringBuilder.append(tmpByte);
		}
		return tmpStringBuilder.toString();
	}
}
