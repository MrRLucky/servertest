package de.luckey.robin.handler;

import java.util.Map;
/**
 * Container for HttpRequests
 * @author Robin Luckey
 */
public class HttpRequest {
	private Map<String, String> headers;
	private String method; 
	private String uri;
	private String version;
	private String body;
	
	/**
	 * Constructs a new HttpRequest container
	 * @param method	 request method
	 * @param uri		 request uri
	 * @param version	 http verison
	 * @param headers	 headers
	 */
	public HttpRequest(String method, String uri, String version, Map<String, String> headers) {
		this.method = method;
		this.uri = uri;
		this.version = version;
		this.headers = headers;
	}

	public Map<String, String> getHeaders() {
		return headers;
	}

	public void setHeaders(Map<String, String> headers) {
		this.headers = headers;
	}

	public String getMethod() {
		return method;
	}

	public void setMethod(String method) {
		this.method = method;
	}

	public String getUri() {
		return uri;
	}

	public void setUri(String uri) {
		this.uri = uri;
	}

	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	public String getBody() {
		return body;
	}

	public void setBody(String body) {
		this.body = body;
	}
	
}
