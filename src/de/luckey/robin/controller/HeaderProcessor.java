package de.luckey.robin.controller;

import static de.luckey.robin.util.Logging.log;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import de.luckey.robin.util.HeadersEnum;
import uk.co.xenonique.http.net.HttpStatusCode;
/**
 * Processes headers of a request to determine response code
 * Currentlyy implemented: If-Match, If-Non-Match, If-Modified-Since
 *  
 * @author Robin Luckey
 *
 */
public class HeaderProcessor {	
	// the headers of the current request
	private Map<String, String> headers;
	// the eTag of the requested entity
	private String eTag;
	// the requested file
	private Date modificationDate;
	
	/**
	 * A constructs a request specific HeaderProcessor
	 * @param headers	headers of the request 
	 * @param eTag	eTag of the requested file
	 * @param requestedFile	the requested file
	 */
	public HeaderProcessor(Map<String, String> headers, String eTag, Date modificationDate) {
		this.headers = headers;
		this.eTag = eTag;
		this.modificationDate = modificationDate;
	}
	
	/**
	 * Processes the headers, only checks If-Match, If-Non-Match, If-Modified-Since
	 * @return the resulting Http Status Code
	 */
	public HttpStatusCode getHttpStatus() {
		if(!noneMatch()) {			
			return HttpStatusCode.NOT_MODIFIED;			
		}
		if(!modifiedSince()) {			
			return HttpStatusCode.NOT_MODIFIED;
		}
		if(!match()) {
			return HttpStatusCode.PRECONDITION_FAILED;
		}
		return HttpStatusCode.OK;
	}
	/**
	 * processes the If-Match header
	 * @return returns true if the If-Match header is not present, one of the eTags provided by the request matches the eTag of the requested file
	 * or if "*"(asterix) is among the eTags of the request and false otherwise.
	 */
	private boolean match() {
		String headerName = HeadersEnum.IF_MATCH.stringValue().trim();
		
		if(!headers.keySet().contains(headerName)) {
			return true;
		}
		
		List<String> list = Arrays.asList(headers.get(headerName).split(" *, *"));
		 
		return list.contains(eTag) || list.contains("*");
	}
	/**
	 * processes the If-None-Match header
	 * @return returns true if the If-None-Match header is not present, none of the eTags provided by the request matches the eTag of the requested File or
	 * if one of the eTags provided by the request is a "*"(asterix) AND the resource is not out of date (as checked by the modifiedSince method) 
	 */
	private boolean noneMatch() {
		String headerName = HeadersEnum.IF_NONE_MATCH.stringValue().trim();
		
		if(!headers.keySet().contains(headerName)) {
			return true;
		}
		
		List<String> list = Arrays.asList(headers.get(headerName).split(" *, *"));
			
		if(list.contains("*")) {
			return true;
		}
		
		return !list.contains(eTag);
	}
	/**
	 * Checks If-Modified header 
	 * @return returns true if the If-Modified header is not present or the modification date of the requested file is older then the date provided by the header
	 */
	private boolean modifiedSince() {
		boolean isModified = true;
		
		String headerName = HeadersEnum.IF_MODIFIED_SINCE.stringValue().trim();
		if(!headers.keySet().contains(headerName)) {
			return true;
		}
		
		SimpleDateFormat format = new SimpleDateFormat("EEE, dd MMM yyyy HH:mm:ss zzz", Locale.ENGLISH);
		Date requestDate = new Date();
		try {
			requestDate = format.parse(headers.get(headerName));
		} catch (ParseException e) {
			// if the date could not be parsed, ignore this header
			log("%s", "Could not parse Date ignoring modifiedSince header");
			return true;
		}
		isModified = modificationDate.after(requestDate);			
		
		return isModified;
	}
}
