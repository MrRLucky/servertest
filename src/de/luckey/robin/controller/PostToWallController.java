package de.luckey.robin.controller;

import java.io.IOException;
import java.io.OutputStream;

import de.luckey.robin.database.MongoDB;
import de.luckey.robin.handler.HttpRequest;
import de.luckey.robin.handler.HttpResponse;
import uk.co.xenonique.http.net.HttpStatusCode;
/**
 * Handles Request that post to the wall
 * @author Robin Luckey
 *
 */
public class PostToWallController implements Controller {
	/**
	 * Calls the Database abstraction to insert the Post into the Database
	 * 
	 *  @param the request containing the post in its body
	 */
	@Override
	public HttpResponse serve(HttpRequest request) {
		
		MongoDB db = MongoDB.getInstance();
		db.insertDocument(request.getBody());
		
		return new HttpResponse(HttpStatusCode.OK.asText()) {
			@Override
			protected void writeBody(OutputStream os) throws IOException {}
			@Override
			protected void addHeaders() {}
		};
	}

}
