package de.luckey.robin.controller;

import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintWriter;

import de.luckey.robin.database.MongoDB;
import de.luckey.robin.handler.HttpRequest;
import de.luckey.robin.handler.HttpResponse;
import uk.co.xenonique.http.net.HttpStatusCode;

public class GetWallpostsController implements Controller{
	@Override
	public HttpResponse serve(HttpRequest request) {
		MongoDB db = MongoDB.getInstance();
		String allPostsJson = db.getAllPosts();
		return new HttpResponse(HttpStatusCode.OK.asText()) {
			@Override
			protected void addHeaders() {}
			
			@Override
			protected void writeBody(OutputStream os) throws IOException {
				PrintWriter pw = new PrintWriter(os);
				pw.write(allPostsJson);
				pw.close();						
			}
		};
	}

}
