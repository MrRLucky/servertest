package de.luckey.robin.controller;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.sql.Date;

import de.luckey.robin.handler.HttpRequest;
import de.luckey.robin.handler.HttpResponse;
import de.luckey.robin.server.Settings;
import de.luckey.robin.util.FileUtils;
import de.luckey.robin.util.HeadersEnum;
import uk.co.xenonique.http.net.HttpStatusCode;

/**
 * An HttpHandler which interprets URIs as files with paths relative to
 * a root directory.
 * @author Johannes Rudolph
 * @author Robin Luckey originally this class was a Handler implementing the HttpHandler and handled all requests. 
 * I changed it to be a Controller and only handle file Requests. 
 * And added the file listing functionality 
 * I also extracted several Methods and put them into the FileUtils interface as static Methods 
 */
public class StaticFileController implements Controller {
    private static final int BUFFERSIZE = 65536;
    private final File root;

    public StaticFileController() {
        super();
        this.root = new File(Settings.getInstance().getRoot());
    }

    /**
     * Generates a Response for File (directory and actual file) requests.
     * If the Uri denotes
     * @params request the request to be processed
     */
	@Override
	public HttpResponse serve(HttpRequest request) {
		File requestedFile = FileUtils.fileByPath(root, request.getUri());
		if(!FileUtils.isFileInsideRoot(root, requestedFile)) {
			return createErrorResponse(HttpStatusCode.FORBIDDEN); 
		}
		if (requestedFile != null && requestedFile.exists() && !requestedFile.isHidden()) {        	
			String eTag = "";
			try {
				eTag = FileUtils.createETAG(requestedFile);
			} catch (IOException e) {
				 return createErrorResponse(HttpStatusCode.INTERNAL_SERVER_ERROR);
			}
			
			Date lastModified = new Date(requestedFile.lastModified());
			
			// Process Headers
			HttpStatusCode headerStatus = new HeaderProcessor(request.getHeaders(), eTag, lastModified).getHttpStatus();
				
			if(request.getMethod().equals("GET")) {
				if(headerStatus.equals(HttpStatusCode.OK)){
					return createResponseWithContent(headerStatus, requestedFile, eTag);
				}
				else {
					return createResponseWithHeaderOnly(headerStatus, eTag);
				}
			}
			else if(request.getMethod().equals("HEAD")) {
					return createResponseWithHeaderOnly(headerStatus, eTag);				
			}
			else {
				return createErrorResponse(HttpStatusCode.NOT_FOUND);
			}			
		}        
		else {
			return createErrorResponse(HttpStatusCode.NOT_FOUND);
		}
	}
	/**
	 * Creates a new HttpResult Object with Headers and a Body used for getRequests with Status 200
	 * the Body can either contain a file or a file listing
	 * @param headerStatus the HttpStatus of the Response
	 * @param f the requested file  or directory
	 * @param eTag the Etag generated from the the fiel or directory
	 * @return the created HttpResponse object
	 */
	private HttpResponse createResponseWithContent(HttpStatusCode headerStatus, File f, String eTag) {
		return new HttpResponse(headerStatus.asText()) {
			@Override
			protected void writeBody(OutputStream os) throws IOException {
				if (f.isDirectory()) {
					PrintWriter pw = new PrintWriter(os);
					String listing = FileUtils.getDirectoryListing(f);
					pw.write(listing);
					pw.close();						
				}
				else {  				
					InputStream is = new FileInputStream(f);					
				
					byte[] buffer = new byte[BUFFERSIZE];
					
					while (is.available() > 0) {
						int read = is.read(buffer);
						os.write(buffer, 0, read);
					}
					is.close();
				}
			}
			@Override
			protected void addHeaders() {
				String length = "";
				if (f.isDirectory()) {
					length = Integer.toString(FileUtils.getDirectoryListing(f).length());
				}
				else {
					length = Long.toString(f.length());
					addResponseHeader("Content-Type", FileUtils.mimeTypeByExtension(f));
				}
				addResponseHeader("Content-Length", length);
				addResponseHeader(HeadersEnum.ETAG.stringValue(), eTag);
			}
		};
	}
	/**
	 * Creates a HttpResponse object containing only Headers used for conditional and head requests
	 * @param headerStatus
	 * @param eTag
	 * @return the created HttpResponse object
	 */
	private HttpResponse createResponseWithHeaderOnly(HttpStatusCode headerStatus, String eTag) {
		return new HttpResponse(headerStatus.asText()) {
			@Override
			protected void addHeaders() {
				addResponseHeader(HeadersEnum.ETAG.stringValue(), eTag);
			}
			@Override
			protected void writeBody(OutputStream os) throws IOException {}
		};
	}
	/**
	 * Creates a HttpResponseObject only containing the HttpResult used for Errors
	 * @param headerStatus
	 * @param eTag
	 * @return the created HttpResponse object
	 */
	private HttpResponse createErrorResponse(HttpStatusCode headerStatus) {
		return new HttpResponse(headerStatus.asText()) {
			@Override
			protected void addHeaders() {}

			@Override
			protected void writeBody(OutputStream os) throws IOException {}
		};
	}
	
}
