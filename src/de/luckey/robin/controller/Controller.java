package de.luckey.robin.controller;

import de.luckey.robin.handler.HttpRequest;
import de.luckey.robin.handler.HttpResponse;

/**
 * A Controller Generates and returns a response object based on the properties of a request object 
 * @author Robin Luckey
 */
public interface Controller {
	/**
	 * Generate a response object based on the properties of a request 
	 * @param request	a http request
	 * @return	a HttpResponse
	 */
    public HttpResponse serve(HttpRequest request);
}
