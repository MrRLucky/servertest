package de.luckey.robin.database;
/**
 * Represents a post on the Wall
 * @author Robin Luckey
 *
 */
public class Post {
	private String name;
	private String message;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getText() {
		return message;
	}

	public void setText(String text) {
		this.message = text;
	}

	public Post(String name, String text) {
		this.name = name;
		this.message = text;
	}
}
