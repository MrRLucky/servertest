package de.luckey.robin.database;

import org.bson.Document;

import com.google.gson.Gson;
import com.mongodb.BasicDBList;
import com.mongodb.MongoClient;
import com.mongodb.MongoClientURI;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoCursor;
import com.mongodb.client.MongoDatabase;
import com.mongodb.util.JSON;

import de.luckey.robin.server.Settings;
/**
 * Singleton class providing a connection and interface to a MongoDB
 * @author Robin Luckey
 *
 */
public class MongoDB {
	
	private static MongoDB instance;
	private MongoDatabase database;
	private MongoClient mongoClient;
	private MongoCollection<Document> posts;
	
	/**
	 * Constructs a new Instance and connects to the MongoDB
	 */
	private MongoDB() {
		mongoClient = new MongoClient(new MongoClientURI("mongodb://"+ Settings.getInstance().getDatabaseHost() + ":" + Settings.getInstance().getDBPort()));
		database = mongoClient.getDatabase("wall");
		posts = database.getCollection("posts");
		// insert example
		if(posts.count() == 0) insertDocument("{\"name\":\"Robin Luckey\",\"message\":\"Welcome to my wall!\"}");
	}
	/**
	 * Inserts a post into the MongoDB posts collection
	 * @param post a Post that must be in Json format
	 */
	public void insertDocument(String jSONPost) {		
		Gson gson = new Gson();

		Post post = gson.fromJson(jSONPost, Post.class);
		// Sanitize
		post.setName(post.getName().replaceAll("[^ a-zA-Z0-9,\\.\\!\\?]", ""));
		post.setText(post.getText().replaceAll("[^ a-zA-Z0-9,\\.\\!\\?]", ""));
		Document document = new Document()
							.append("name", post.getName())
							.append("message", post.getText());
		posts.insertOne(document);
	}
	/**
	 * creates new instance of the MongoDB class or returns the instance if it already exists
	 * @return an instance of this class
	 */
	public static MongoDB getInstance() {
		if(instance == null) {
			synchronized (MongoDB.class) {
				if(instance == null) {
					instance = new MongoDB();
				}
			}
		}
		return instance;
	}
	/**
	 * returns all post as Array in Json Format
	 * @return
	 */
	public String getAllPosts() {
		FindIterable<Document> find = posts.find().limit(10);
		MongoCursor<Document> iterator = find.iterator();
		BasicDBList list = new BasicDBList();	
        while(iterator.hasNext()) {
        	Document doc = iterator.next();
            list.add(doc);
		}
        return JSON.serialize(list);
	}
}