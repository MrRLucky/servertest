package de.luckey.robin.util;
/**
 * @author Johannes Rudolph
 * @author Robin Luckey changed from class to interface
 */
public interface Logging {
    public static void log(String format, Object... args) {
        System.out.printf(format+"\n", args);
    }
}
