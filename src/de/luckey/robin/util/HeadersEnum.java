// LICENSE GPL 3.0 Peter Pilgrim, Xenonique.co.uk, 2012
// http://www.gnu.org/licenses/gpl.html GNU PUBLIC LICENSE 3.0
package de.luckey.robin.util;

/**
 * Enum of request headers
 * 
 */
public enum HeadersEnum {
	ETAG("ETag"),
	EXPIRES("Expires"),
	LAST_MODIFIED("Last-Modified"),
	IF_MATCH("If-Match"),
	IF_NONE_MATCH("If-None-Match"),
	IF_MODIFIED_SINCE("If-Modified-Since"),
	CONTENT_LENGTH("Content-Length");
	
    private String stringValue;

	private HeadersEnum(String stringValue) {
        this.stringValue = stringValue;
    }

    /**
     * Gets the Header string value
     * @return the Header string value
     */
    public String stringValue() {
        return stringValue;
    }
}