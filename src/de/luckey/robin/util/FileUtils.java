package de.luckey.robin.util;

import static de.luckey.robin.util.Logging.log;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.FileSystem;
import java.nio.file.FileSystems;
import java.security.DigestInputStream;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import javax.xml.bind.annotation.adapters.HexBinaryAdapter;

import de.luckey.robin.server.Settings;
/**
 * Interface that provides static methods for File Handling
 * @author Robin Luckey
 *
 */
public interface FileUtils {
	/**
	 * Calculates an md5 hash of the File and returns it as String
	 * @param f the input file
	 * @return return an md5 hash as String
	 * @throws IOException 
	 */
	public static String createETAG(File f) throws IOException {
		if(f.isDirectory()){
			int hash = (f.getCanonicalPath() + f.lastModified()).hashCode();
			String eTag = ("W/\"" + Integer.toHexString(hash) + "\"").toLowerCase();
			return  eTag;
		}
		else {
			byte[] digest = null;
			try {		
				InputStream is = new FileInputStream(f); 
				MessageDigest md = MessageDigest.getInstance("MD5");
				DigestInputStream dis = new DigestInputStream(is, md);
				while (dis.available() > 0) {
					dis.read();
				}
				dis.close();
				digest = md.digest();
			} catch (IOException e) {
				System.out.println("Could not read file");
			} catch (NoSuchAlgorithmException e) {
				System.out.println(e);
			}
			
			String hex = (new HexBinaryAdapter()).marshal(digest).toLowerCase();				
			return hex; 		
		}
	}
	/**
	 * Creates a List of all Files and folders in Directory
	 * @param f
	 * @return
	 */
	public static String getDirectoryListing(final File f) {
		File[] listOfFiles = f.listFiles();
		String listing = "Directory listing: \n";
		
		for (File file : listOfFiles) {
			listing += file.getName() + "\n";
		}
		return listing;
	}
	
	/**
	 *  
	 * @param path
	 * @return returns null if canonical path is not within root directory
	 */
	public static File fileByPath(File root, String path) {
        /* FIXME: This is a potential security risk.
         * In a real application, you would have to make
         * sure that you effectively restrict the scope of
         * accessible files to the ones intended by excluding
         * the parent path (..) and symbolic links etc.
         */
    	// strip off leading slashes
    	while(path.startsWith("/"))
    		path = path.substring(1);
    	

        return new File(root, path);
    }
	/**
	 * Checks if requested file is inside the webroot directory 
	 * @param root the webroot directory
	 * @param path the requested file taken from the uri 
	 * @return true if canonical path of file is inside the webroot
	 */
	public static boolean isFileInsideRoot(File root, File requestedFile) {
		//check if file is inside root
    	boolean pathAllowed = false;
    	try {
    		String fPath = requestedFile.getCanonicalPath();
    		FileSystem defaultFileSystem = FileSystems.getDefault();
    		if(fPath.endsWith("/")) {
    			fPath+= defaultFileSystem.getSeparator();
    		}
    		pathAllowed = requestedFile.getCanonicalPath().startsWith(root.getCanonicalPath());
		} catch (IOException e) {
			log("%s", "Could not retrieve cononical path");
		}
		return pathAllowed;
	}
	/**
	 * Determines the media type of a file based on its ending
	 * endings that are recognized:
	 * html
	 * js
	 * @param f the file 
	 * @return a String containing the type or "application/octet-stream" if file ending is not recognized.
	 */
	public static String mimeTypeByExtension(File f) {
        String name = f.getName();
        String extension = name.substring(name.lastIndexOf(".") + 1);
        if ("html".equals(extension))
            return "text/html";
        else if ("js".equals(extension)) {
        	return "application/javascript";
        }
        else
            return "application/octet-stream";
    }
	/**
	 * creates a directory relative to the execution directory of the server  
	 * @param path the path to the directory
	 */
	public static String createDir(String path) {
		File directory = new File(Settings.getInstance().getRoot());
		boolean success = directory.mkdirs();
		String canonicalPath = "";
		try {
			canonicalPath = directory.getCanonicalPath();
		} catch (IOException e) {
			log("%s", "Could not get canonical path.");
		}
		if(success) { 				
			Logging.log("Created directory at %s", !canonicalPath.equals("")? canonicalPath : path);			
		}
		return !canonicalPath.equals("")? canonicalPath : path;
	}
}
